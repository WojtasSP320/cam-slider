
#include <Encoder.h>

#define ENC_BUTTON_PIN          5
#define ENC_DATA_PIN            3
#define ENC_CLK_PIN             4

Encoder encoder(ENC_DATA_PIN, ENC_CLK_PIN);

class Knob {
  private:
    long previousValue = 0;
    bool previousButton = false;
  public:
    bool hasChangedValue = false;
    bool hasChangedButton = false;
    bool isPressed = false;
    long value = 0;

    void begin() {
      pinMode(ENC_BUTTON_PIN, INPUT);
      digitalWrite(ENC_BUTTON_PIN, HIGH);
    }

    void read() {
      value = encoder.read();
      isPressed = digitalRead(ENC_BUTTON_PIN) == 0;

      if (value != previousValue) {
        hasChangedValue = true;
        previousValue = value;
      }

      if (isPressed != previousButton) {
        hasChangedButton = true;
        previousButton = isPressed;
      }
    }

    void write(int32_t position) {
        encoder.write(position);
        value = position;
        previousValue = position;
    }

    void clearChanges() {
        hasChangedValue = false;
        hasChangedButton = false;
    }
};
