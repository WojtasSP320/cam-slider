#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);

uint8_t checkMark[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t returnArrow[8] = { 0b00000010, 0b00000110, 0b00001110, 0b00011110, 0b00001110, 0b00000110, 0b00000010 };

uint8_t CHECK = 0;
uint8_t RETURN = 1;

class Display {
  public:
     void begin() {
        lcd.begin();
        lcd.clear();
        lcd.backlight();
        lcd.setCursor(0,0);
        
        lcd.createChar(CHECK, checkMark);
        lcd.createChar(RETURN, returnArrow);
     }  

     void printCarriageAlignmentMessage() {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Trwa resetowanie");
        lcd.setCursor(0,1);
        lcd.print("pozycji wozka...");
    } 

     void printKeyboard() {
        lcd.setCursor (0, 1);
        lcd.print("                ");
    
        lcd.setCursor(0, 1);
        lcd.write(RETURN);
    
        lcd.setCursor(5, 1);
        lcd.print("-");
    
        lcd.setCursor(10, 1);
        lcd.print("+");
    
        lcd.setCursor(15, 1);
        lcd.write(CHECK);
    }

    void printPhotosCountScreen(uint16_t photosCount) {
        printKeyboard();
        lcd.setCursor (0, 0);
        lcd.print("ile zdjec:      ");
        lcd.setCursor (12, 0);
        lcd.print(photosCount, DEC);  
    }

    void printSlideTimeScreen(uint16_t slideTime) {
        printKeyboard();
        lcd.setCursor (0, 0);
        lcd.print("czas:           ");
        lcd.setCursor (6, 0);
        lcd.print(slideTime, DEC);
        lcd.print("s");
    }

    void printDirectionScreen(bool direction) {
        printKeyboard();
        lcd.setCursor (0, 0);
        lcd.print("kierunek:       ");
        lcd.setCursor (10, 0);
        if (direction == DIRECTION_LEFT) {
          lcd.print("lewo");
        } else {
          lcd.print("prawo");  
        }
    }

    void printShutterSpeedScreen(uint16_t shutterSpeed) {
        printKeyboard();
        lcd.setCursor (0, 0);
        lcd.print("migawka:        ");
        lcd.setCursor (9, 0);
        lcd.print(shutterSpeed, DEC);
        lcd.print("s");
    }
};
