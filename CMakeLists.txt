cmake_minimum_required(VERSION 3.7)

set(CMAKE_TOOLCHAIN_FILE cmake/ArduinoToolchain.cmake)

set(CMAKE_CXX_STANDARD 11)

project(Blink)

set(SOURCE_FILES slider.ino)

set(${CMAKE_PROJECT_NAME}_SRCS ${SOURCE_FILES})
set(${CMAKE_PROJECT_NAME}_BOARD nano)
# Explicity define board's properties
set(nano.build.mcu atmega328p)
set(nano.upload.speed 57600)

set(${PROJECT_NAME}_BOARD nano)
#set(nano.build.mcu atmega328p)
set(ARDUINO_CPU atmega328old)

GENERATE_ARDUINO_FIRMWARE(${CMAKE_PROJECT_NAME})