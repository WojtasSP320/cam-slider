#define FOCUS_PIN         13
#define SHUTTER_PIN       12

#define SHUTTER_UP        0 //No photo
#define SHUTTER_DOWN      1 //Taking photo
#define SHUTTER_OFF       2 //Timer off
#define SHUTTER_PRE_SHOT  3 //Before actual shot
#define SHUTTER_POST_SHOT 4 //After actual shot

class Shutter {
  private:
    uint32_t shutterCounter = 0;
  public:
    uint32_t shutterUpTime = 0;
    uint32_t shutterDownTime = 0;
    uint8_t photosTaken = 0;
    uint8_t shutterState;
    bool shutterStateChanged = false;
  
    void begin() {
      pinMode(FOCUS_PIN, OUTPUT);
      digitalWrite(FOCUS_PIN, LOW);
      pinMode(SHUTTER_PIN, OUTPUT);
      digitalWrite(SHUTTER_PIN, LOW);

      shutterState = SHUTTER_OFF;
    }

    void shutterThread() {
      if (shutterState != SHUTTER_OFF) {
        //Serial.print(shutterCounter); Serial.print(" / "); Serial.println(shutterUpTime+shutterDownTime);
        if (shutterCounter < shutterUpTime+shutterDownTime) {
          
          if (shutterCounter == 1) {
             shutterState = SHUTTER_PRE_SHOT;
             shutterStateChanged = true;
             digitalWrite(FOCUS_PIN, HIGH);
          } 

          if (shutterCounter == 10) {
             shutterDown();
             shutterStateChanged = true;
          } 
          
          if (shutterCounter == shutterDownTime+10) {
            shutterUp();
            shutterState = SHUTTER_POST_SHOT;
            shutterStateChanged = true;
          }

          /* 10 x 100ms after taking photo idle */

          if (shutterCounter == shutterDownTime + 10 + 10) {
             shutterState = SHUTTER_UP;
             shutterStateChanged = true;
             photosTaken++;
          }
          
          shutterCounter++;
        } else {
          shutterCounter = 1; // Restart counter  
        }
      }
    }

    void shutterDown() {
      digitalWrite(SHUTTER_PIN, HIGH);
      shutterState = SHUTTER_DOWN;
    }

    void shutterUp() {
      digitalWrite(SHUTTER_PIN, LOW);
      digitalWrite(FOCUS_PIN, LOW);
      shutterState = SHUTTER_UP;
    }

    void shutterOn(uint32_t _upTime, uint32_t _downTime) {
      shutterCounter = 1;
      shutterUpTime = _upTime;
      shutterDownTime = _downTime;
      shutterState = SHUTTER_UP;
      shutterStateChanged = true;
    }

    void shutterOff() {
      shutterState = SHUTTER_OFF;  
      shutterStateChanged = true;
    }

    uint8_t getPhotosTaken() {
      return photosTaken;  
    }

    void resetPhotosTaken() {
        photosTaken = 0;
    }
};
