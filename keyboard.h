#define KEY_1_PIN   9
#define KEY_2_PIN   8
#define KEY_3_PIN   11
#define KEY_4_PIN   10

#define KEY_1       1
#define KEY_2       2
#define KEY_3       3
#define KEY_4       4

class Keyboard {
    private:
      bool keyPressed = false;
      uint8_t keys = 0b00001111;
    public:
      void begin() {
          pinMode(KEY_1_PIN, INPUT);
          digitalWrite(KEY_1_PIN, HIGH);
          
          pinMode(KEY_2_PIN, INPUT);
          digitalWrite(KEY_2_PIN, HIGH);
          
          pinMode(KEY_3_PIN, INPUT);
          digitalWrite(KEY_3_PIN, HIGH);
          
          pinMode(KEY_4_PIN, INPUT);
          digitalWrite(KEY_4_PIN, HIGH);
      }

      void printKeys() {
          uint8_t key = getPressedKey();
          if (key == KEY_1) {Serial.println("x___");}
          if (key == KEY_2) {Serial.println("_x__");}
          if (key == KEY_3) {Serial.println("__x_");}
          if (key == KEY_4) {Serial.println("___x");}
      }

      uint8_t getPressedKey() {
          if ((keys & 1) == 0) {return KEY_1;}
          if ((keys & 2) == 0) {return KEY_2;}
          if ((keys & 4) == 0) {return KEY_3;}
          if ((keys & 8) == 0) {return KEY_4;}
      
          return 0;
      }

      void readKeys() {
          keys = (digitalRead(KEY_4_PIN) << 3) + (digitalRead(KEY_3_PIN) << 2) + (digitalRead(KEY_2_PIN) << 1) + digitalRead(KEY_1_PIN);
        
          if (keys < 15) {
            keyPressed = true;
          } else {
            keyPressed = false;
          }
      }

      bool isKeyPressed() {
         return keyPressed;  
      }
      
      void clearKeys() {
          keys = 0b00001111;
          keyPressed = false;  
      }
};
