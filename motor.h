#include <Timers.h>

#define MOTOR_STEP_PIN      15
#define MOTOR_DIRECTION_PIN 14
#define MOTOR_ENABLED_PIN   16
#define START_POSITION_PIN  6
#define STOP_POSITION_PIN   7

#define SLIDER_STEPS_LENGTH 172400UL

typedef void(*threadProcess)(void);

class Motor {
  public:
    bool isDirectionLeft = false;
    bool isEnabled = false;
    bool isMotorOn = false; 
    uint32_t steps = 0;
    bool isAtStartPosition = false;
    bool isAtStopPosition = false;
  
    void begin() {
      pinMode(MOTOR_STEP_PIN, OUTPUT);
      digitalWrite(MOTOR_STEP_PIN, LOW); 
      
      pinMode(MOTOR_DIRECTION_PIN, OUTPUT);
      digitalWrite(MOTOR_DIRECTION_PIN, LOW); 
      
      pinMode(MOTOR_ENABLED_PIN, OUTPUT);
      digitalWrite(MOTOR_ENABLED_PIN, LOW);

      pinMode(START_POSITION_PIN, INPUT);
      digitalWrite(START_POSITION_PIN, HIGH);      
      
      pinMode(STOP_POSITION_PIN, INPUT);
      digitalWrite(STOP_POSITION_PIN, HIGH);
    }

    void motorThread() {
      isAtStartPosition = false;
      isAtStopPosition = false;
      
      if (isDirectionLeft == true) {
        digitalWrite(MOTOR_DIRECTION_PIN, LOW);
      } else {
        digitalWrite(MOTOR_DIRECTION_PIN, HIGH);
      }

      if (isEnabled == true) {
        digitalWrite(MOTOR_ENABLED_PIN, LOW);  
      } else {
        digitalWrite(MOTOR_ENABLED_PIN, HIGH);
      }

       if (isDirectionLeft && LOW == digitalRead(START_POSITION_PIN)) {
        steps = 0;
        isAtStartPosition = true;
      }      
      
      if (false == isDirectionLeft && LOW == digitalRead(STOP_POSITION_PIN)) {
        steps = 0;
        isAtStopPosition = true;
      }

      if (steps > 0) {
        if (isMotorOn == true) {
          digitalWrite(MOTOR_STEP_PIN, LOW);
          isMotorOn = false;
        } else {
          digitalWrite(MOTOR_STEP_PIN, HIGH);  
          isMotorOn = true;
        }

        steps--;
      }

       if (steps == 0) {
          digitalWrite(MOTOR_STEP_PIN, LOW);
          isMotorOn = false;
          //disable();
      }
    }

    void alignCarriageLeft(threadProcess callback) {
      Serial.print(SLIDER_STEPS_LENGTH);
      Serial.println("Rotate left");
      rotateLeft(SLIDER_STEPS_LENGTH);
      while (steps > 0) {
        callback();
     }
        
      Serial.println("Carriage alignment left done.");
    }

    void alignCarriageRight(threadProcess callback) {
      Serial.print(SLIDER_STEPS_LENGTH);
      Serial.println("Rotate right");
      rotateLeft(SLIDER_STEPS_LENGTH);
      while (steps > 0) {
        callback();
     }
        
      Serial.println("Carriage alignment right done.");
    }
    
    void rotateLeft(uint32_t _steps){
        isDirectionLeft = true;
        enable();
        steps = _steps;
    }
    
    void rotateRight(uint32_t _steps){
        isDirectionLeft = false;
        enable();
        steps = _steps;
    }

    void stepOneOfNthLeft(uint32_t _steps) {
       uint32_t distance = (uint32_t)(SLIDER_STEPS_LENGTH / (_steps*1.0));
       Serial.print("Rotate left steps: ");
       Serial.println(distance);
       rotateLeft(distance);
    }

    void stepOneOfNthRight(uint32_t _steps) {
       uint32_t distance = (uint32_t)(SLIDER_STEPS_LENGTH / (_steps*1.0));
       Serial.print("Rotate right steps: ");
       Serial.println(distance);
       rotateRight(distance);
    }
    
    void enable() {
      isEnabled = true;
    }
    
    void disable() {
      isEnabled = false;
    }
};
