#include <Timers.h>
#include <Wire.h>
#include <math.h>

#define DIRECTION_LEFT          false
#define DIRECTION_RIGHT         true

#include "motor.h"
#include "shutter.h"
#include "keyboard.h"
#include "display.h"
#include "knob.h"

#define BATTERY_VOLTAGE_PIN     A3

#define SLIDE_TOTAL_TIME        172400UL
#define SLIDE_TOTAL_STEPS       172400UL
#define STEP_UNDEFINED          0
#define STEP_DIRECTION          1
#define STEP_PHOTOS_COUNT       2
#define STEP_SLIDE_TIME         3
#define STEP_SHUTTER_SPEED      4
#define STEP_COUNTDOWN          5
#define STEP_SLIDE              6
#define STEP_DONE               7

#define THREAD_KEYBOARD         0
#define THREAD_COUNTDOWN        1
#define THREAD_SLIDER           2
#define THREAD_SHUTTER          3
#define THREAD_STEP_TRANSITION  4
#define THREAD_MOTOR            5
#define THREAD_ENCODER          6

uint8_t step = STEP_PHOTOS_COUNT;

float batteryLevel;

Timers <8> thread;
typedef void(*callb)(void);

callb nextStep;

Motor motor;
Shutter shutter;
Keyboard keyboard;
Display display;
Knob knob;

void(* softReset) (void) = 0;

bool shootToggle = false;
uint8_t countdown = 0;

int16_t photosCount = 1200;
int16_t photosTaken = 0;
int32_t slideTime = 3600;
int16_t shutterSpeed = 1;
bool direction = DIRECTION_LEFT;

int16_t minPhotosCount = 2;
int16_t maxPhotosCount = 10775;
int16_t minShutterSpeed = 1;
int32_t maxShutterSpeed = 3600;
int16_t minSlideTime = 20;
int32_t maxSlideTime = 86400;


void directionScreen() {
    step = STEP_DIRECTION;
    uint8_t key = keyboard.getPressedKey();
    keyboard.clearKeys();
    
    if (key == KEY_2) {direction = DIRECTION_RIGHT;}
    if (key == KEY_3) {direction = DIRECTION_LEFT;}
    if (key == KEY_4 || knob.isPressed) { goToStep(photosCountScreen, 500); return;}

    display.printDirectionScreen(direction);
}

void photosCountScreen() {
    step = STEP_PHOTOS_COUNT;
    uint8_t key = keyboard.getPressedKey();
    keyboard.clearKeys();

    if (key == KEY_1) {step--;}
    if (key == KEY_2 && photosCount > minPhotosCount) {photosCount--;}
    if (key == KEY_3 && photosCount <= maxPhotosCount) {photosCount++;}
    if (key == KEY_4 || knob.isPressed) {goToStep(slideTimeScreen, 500); return;}

    display.printPhotosCountScreen(photosCount);
}

void slideTimeScreen() {
    step = STEP_SLIDE_TIME;
    uint8_t key = keyboard.getPressedKey();
    keyboard.clearKeys();
    
    if (key == KEY_1) {step--;}
    if (key == KEY_2 && slideTime > minSlideTime) {slideTime--;}
    if (key == KEY_3 && slideTime < maxSlideTime) {slideTime++;}
    if (key == KEY_4 || knob.isPressed) {
      step++; 
      minShutterSpeed = 0;
      maxShutterSpeed = floor(slideTime/(photosCount*1.0) - 2.0 - ceil(SLIDE_TOTAL_TIME/1000.0/(photosCount*1.0)));

      Serial.print("Time per shoot: "); Serial.println(slideTime/(photosCount*1.0));
      Serial.print("Time needed for move: "); Serial.println(SLIDE_TOTAL_TIME/1000.0/(photosCount*1.0));
      Serial.print("Max shutter speed: "); Serial.println(maxShutterSpeed);
      lcd.clear(); 
      if (maxShutterSpeed >= 1) {
        goToStep(shutterSpeedScreen, 500); 
      } else {
        step--;
        goToStep(slideTimeScreen, 500);  
      }
      return;
    }

    display.printSlideTimeScreen(slideTime);
}

void shutterSpeedScreen() {
    step = STEP_SHUTTER_SPEED;
    uint8_t key = keyboard.getPressedKey();
    keyboard.clearKeys();
    
    if (key == KEY_1) {step--;}
    if (key == KEY_2 && shutterSpeed > minShutterSpeed) {shutterSpeed--;}
    if (key == KEY_3 && shutterSpeed < maxShutterSpeed) {shutterSpeed++;}
    if (key == KEY_4 || knob.isPressed) {
      display.printCarriageAlignmentMessage();
      if (direction == DIRECTION_RIGHT) {
        motor.alignCarriageLeft(proc);  
      } else {
          motor.alignCarriageRight(proc);
      }
      
      step++; 
      lcd.clear(); 
      countdown = 3; 
      thread.setInterval(THREAD_COUNTDOWN, 1000); 
      return;
    }

    display.printShutterSpeedScreen(shutterSpeed);
}

void goToStep(callb callback, uint16_t timeout) {
    step = STEP_UNDEFINED;
    lcd.clear();
    Serial.println("setting callback");
    nextStep = callback;
    thread.setInterval(THREAD_STEP_TRANSITION, timeout);
}

void stepTransition() {
    Serial.println("step transition");
    thread.setInterval(THREAD_STEP_TRANSITION, 0);
    Serial.println("Going to next step");
    nextStep();
}

void motorThread() {
  motor.motorThread();  
}

void shutterThread() {
  shutter.shutterThread();  
}

void encoderThread() {
  knob.read();

  if (step == STEP_DIRECTION) {
      direction = knob.value < 0;
  }
  
  if (step == STEP_PHOTOS_COUNT) {
      photosCount -= knob.value*5;
      if (photosCount < minPhotosCount) {photosCount = minPhotosCount;}
      if (photosCount > maxPhotosCount) {photosCount = maxPhotosCount;}
  }

  if (step == STEP_SLIDE_TIME) {
     slideTime -= knob.value*30;
     if (slideTime < minSlideTime) {slideTime = minSlideTime;}
     if (slideTime > maxSlideTime) {slideTime = maxSlideTime;}
  }

  if (step == STEP_SHUTTER_SPEED) {
    shutterSpeed -= (long)(knob.value*0.5);
    if (shutterSpeed < minShutterSpeed) {shutterSpeed = minShutterSpeed;}
    if (shutterSpeed > maxShutterSpeed) {shutterSpeed = maxShutterSpeed;}
  }

  knob.write(0);
}

void done() {
  step = STEP_DONE;
  thread.setInterval(THREAD_MOTOR, 0); 
  shutter.shutterOff();
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Zdjecia zrobione");
  Serial.println("Done!");
  
  lcd.setCursor(0,1);
  lcd.print(photosTaken, DEC);
  lcd.print(" z ");  
  lcd.print(photosCount, DEC);
  Serial.print("Photos taken: ");  Serial.print(photosTaken);  Serial.print(" of ");  Serial.println(photosCount);
}

void stop() {
  step = STEP_DONE;
  thread.setInterval(THREAD_MOTOR, 0); 
  shutter.shutterOff();
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("  !!! STOP !!!  ");
  Serial.println("Stop!");

  lcd.setCursor(0,1);
  lcd.print(photosTaken, DEC);
  lcd.print(" z ");  
  lcd.print(photosCount, DEC);
  Serial.print("Photos taken: ");  Serial.print(photosTaken);  Serial.print(" of ");  Serial.println(photosCount);
}

void countDown() {
  lcd.setCursor(0, 0);

  lcd.print(photosCount, DEC);
  lcd.print(" ");
  lcd.print(slideTime, DEC);
  lcd.print("s [");
  lcd.print(shutterSpeed, DEC);
  lcd.print("s]");
  
  lcd.setCursor(0, 1);
  lcd.print("Start za: ");
  lcd.print(countdown, DEC);

  if (countdown == 0) {
    thread.setInterval(THREAD_COUNTDOWN, 0);  
    
    beginSlide();
    return;
  }

  countdown--;
}

void beginSlide() {
    step = STEP_SLIDE;

    uint32_t downTime = (shutterSpeed*10);
    uint32_t upTime = (uint32_t)((slideTime*10)/photosCount - downTime);

    Serial.print("Steps per photo:"); Serial.println(SLIDE_TOTAL_STEPS/photosCount);
    Serial.print("Photos count: "); Serial.println(photosCount);
    Serial.print("Shutter up time: "); Serial.print(upTime*100); Serial.println("ms");
    Serial.print("Shutter down time: "); Serial.print(downTime*100); Serial.println("ms");

    thread.setInterval(THREAD_SHUTTER, 100);

    shutter.shutterOn(upTime, downTime);
}

void readKeysCallback() {
  keyboard.readKeys();
}

void proc() {
  thread.process();  
}

float getBatteryLevel() {
  return analogRead(BATTERY_VOLTAGE_PIN) / 1023.0 / (1.0 / 5.68) * 5.0;  
}

/**
 * Setup function
 */
void setup() { 
    Wire.begin();
    Serial.begin(115200);
    display.begin();

    batteryLevel = getBatteryLevel();

    Serial.print(batteryLevel, 3);
    Serial.println(" V");

    shutter.begin();
    keyboard.begin();
    motor.begin();
    knob.begin();
    
    thread.attach(THREAD_KEYBOARD, 50, readKeysCallback);
    thread.attach(THREAD_COUNTDOWN, 0, countDown);
    thread.attach(THREAD_SHUTTER, 0, shutterThread);
    thread.attach(THREAD_STEP_TRANSITION, 0, stepTransition);
    thread.attach(THREAD_MOTOR, 1, motorThread);
    knob.write(0);
    thread.attach(THREAD_ENCODER, 50, encoderThread);
    directionScreen();
}

/**
 * Main loop function
 */
void loop() {
    thread.process();
   
    if (step == STEP_SLIDE) {
        if (shutter.getPhotosTaken() > 0) {
          photosTaken += shutter.getPhotosTaken();
          shutter.resetPhotosTaken();
          
          lcd.setCursor(0, 1);
          lcd.print(photosTaken, DEC);
          lcd.print("/");
          lcd.print(photosCount, DEC);
          lcd.print("              ");

          
          batteryLevel = getBatteryLevel();
          Serial.print(batteryLevel, 3);
          Serial.println(" V");
          lcd.setCursor(10, 1);
          lcd.print(batteryLevel,2);
          lcd.print("V");

          if (photosTaken == photosCount) {
            done(); 
            return; 
          }

          if (direction == DIRECTION_RIGHT) {
            motor.stepOneOfNthRight(photosCount-1);

            if (motor.isAtStopPosition == true) {
              stop();  
              return;
            }
          } else {
            motor.stepOneOfNthLeft(photosCount-1);

            if (motor.isAtStopPosition == true) {
              stop();  
              return;
            }
          }
        }

        if (shutter.shutterState != SHUTTER_OFF && shutter.shutterStateChanged) {
          
          switch (shutter.shutterState) {
            case SHUTTER_PRE_SHOT:
              lcd.setCursor (0, 0);
              thread.process();
              lcd.print("      ____      ");
              break;
            case SHUTTER_DOWN:
              lcd.setCursor (0, 0);
              thread.process();
              lcd.print("      ****      ");
              break;
            case SHUTTER_POST_SHOT:
              lcd.setCursor (0, 0);
              thread.process();
              lcd.print("      ____      ");
              break;
            case SHUTTER_UP:
              lcd.setCursor (0, 0);
              thread.process();
              lcd.print("                ");
              break;
          }

          shutter.shutterStateChanged = false;
        }
        
        return;  
    }

     if (keyboard.isKeyPressed() || knob.hasChangedValue || knob.hasChangedButton) {
        switch (step) {
           case STEP_DIRECTION:
                directionScreen();
                break;
            case STEP_PHOTOS_COUNT:
                photosCountScreen();
                break;
            case STEP_SLIDE_TIME:
                slideTimeScreen();
                break;
            case STEP_SHUTTER_SPEED:
                shutterSpeedScreen();
                break;
            case STEP_DONE:
                softReset();
                break;    
        }
    }

    knob.clearChanges();
}
